# OpenML dataset: Heart_Failure_Prediction_-_Clinical_Records_

https://www.openml.org/d/45949

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "heart_failure_clinical_records.csv" dataset comprises clinical records of patients with heart failure, detailing various medical attributes that may contribute to heart failure incidents. This dataset is instrumental for researchers and healthcare professionals aiming to analyze factors leading to heart failure and mortality. The collected data spans individuals of varied ages, with measurements such as anaemia presence, creatinine phosphokinase levels, diabetes status, ejection fraction, high blood pressure, platelets count, serum creatinine, serum sodium levels, sex, smoking status, follow-up period (time), and the event of death.

Attribute Description:
- age: Age of the patient (years)
- anaemia: Decrease of red blood cells or hemoglobin (0: No, 1: Yes)
- creatinine_phosphokinase: Level of the CPK enzyme in the blood (mcg/L)
- diabetes: If the patient has diabetes (0: No, 1: Yes)
- ejection_fraction: Percentage of blood leaving the heart at each contraction (%)
- high_blood_pressure: If the patient has hypertension (0: No, 1: Yes)
- platelets: Platelets in the blood (kiloplatelets/mL)
- serum_creatinine: Level of serum creatinine in the blood (mg/dL)
- serum_sodium: Level of serum sodium in the blood (mEq/L)
- sex: Biological sex of the patient (0: Female, 1: Male)
- smoking: If the patient smokes (0: No, 1: Yes)
- time: Follow-up period (days)
- DEATH_EVENT: If the patient deceased during the follow-up period (0: No, 1: Yes)

Use Case:
This dataset can serve a vital role in machine learning projects and statistical analyses aiming to predict heart failure mortality and understand the impact of various predictors on heart failure outcomes. Researchers can use this data to develop predictive models, identify key risk factors, and propose targeted interventions for at-risk populations. Public health officials and policy makers can also leverage the insights gained to guide healthcare resource allocation and preventive strategies.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45949) of an [OpenML dataset](https://www.openml.org/d/45949). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45949/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45949/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45949/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

